package com.privasia.jpa;

import javax.persistence.Column;
import javax.persistence.Transient;
import java.util.Date;
import java.util.Objects;

@lombok.Getter
@lombok.Setter
@javax.persistence.Entity
@javax.persistence.Table(name = "rd_tug_gps")
public class TUG_GPS implements java.io.Serializable {

    private static final long serialVersionUID = 1234L;

    @javax.persistence.Id
    @Column(name = "ts_datetime", nullable = false)
    private Date datetime;

    @Column(nullable = false)
    private Date ts_update = new Date();

    @Column(nullable = false)
    private String name;

    @javax.persistence.Id
    @Column(nullable = false)
    private Integer mmsi;

    @javax.persistence.Id
    @Column(nullable = false)
    private double lon;

    @javax.persistence.Id
    @Column(nullable = false)
    private double lat;

    @Transient
    public void setGPS(double longitude, double latitude) {
        lon = longitude;
        lat = latitude;
    }

    public TUG_GPS() {
    }

    public TUG_GPS(int tugMMSI, Date dt, String boat, double longitude, double latitude) {
        mmsi = tugMMSI;
        datetime = dt;
        name = boat;
        lon = longitude;
        lat = latitude;
    }

    public TUG_GPS(int tugMMSI) {
        mmsi = tugMMSI;
    }

    @Override
    public String toString() {
        return datetime.getTime() + "," + mmsi + " " + name + " (" + lon + "," + lat + ")";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TUG_GPS tug_gps = (TUG_GPS) o;
        return Double.compare(tug_gps.lon, lon) == 0 &&
                Double.compare(tug_gps.lat, lat) == 0 &&
                Objects.equals(datetime, tug_gps.datetime) &&
                Objects.equals(mmsi, tug_gps.mmsi);
    }

    @Override
    public int hashCode() {
        return Objects.hash(datetime, mmsi, lon, lat);
    }
}
