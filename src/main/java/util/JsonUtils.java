package util;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.util.List;

public class JsonUtils {

    public static <T> T toArray(File file, TypeReference<T> typeRefArray) {
        ObjectMapper m = new ObjectMapper();
        try {
            return m.readValue(file, typeRefArray);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return null;
    }

    public static <T> List<T> toList(String json, TypeReference<List<T>> typeRefList) {
        ObjectMapper m = new ObjectMapper();
        try {
            return m.readValue(json, typeRefList);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return null;
    }

    public static <T> List<T> toList(File file, TypeReference<List<T>> typeRefList) {
        ObjectMapper m = new ObjectMapper();
        try {
            return m.readValue(file, typeRefList);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return null;
    }

    public static <T> T toOBJ(File file, Class<T> o) {
        ObjectMapper m = new ObjectMapper();
        try {
            return m.readValue(file, o);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return null;
    }

    public static <T> T toOBJ(String json, Class<T> o) {
        ObjectMapper m = new ObjectMapper();
        try {
            return m.readValue(json, o);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return null;
    }

    public static String toJSON(Object o) {
        return toJSON(o, true);
    }

    public static String toJSON(Object o, boolean pretty) {
        ObjectMapper m = new ObjectMapper();
        try {
            return pretty ? m.writerWithDefaultPrettyPrinter().writeValueAsString(o) : m.writeValueAsString(o);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return "";
    }

    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(JsonUtils.class);
}

