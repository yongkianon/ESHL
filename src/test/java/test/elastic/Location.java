package test.elastic;

@lombok.Getter
@lombok.Setter
public class Location {

    public Location(double longitude, double latitude) {
        lon = longitude; // X
        lat = latitude;  // Y
    }

    private double lon;
    private double lat;

    @Override
    public String toString() {
        return "[" + lon + "," + lat + "]";
    }
}
