package test.elastic;

import util.JsonUtils;

import java.util.Date;

@lombok.Getter
@lombok.Setter
public class ElasticJSON {

    public static void main(String[] args) {
        println(JsonUtils.toJSON(new ElasticJSON()));
    }

    private String name = "YONG";
    private Location location = new Location(3.001003, 101.001002);

    private Date ts = new Date();
    private long ms = ts.getTime();

    private static void println(Object o) {
        System.out.println(o);
    }
}
