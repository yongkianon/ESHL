package test.elastic;

import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.RequestLine;
import org.apache.http.util.EntityUtils;
import org.elasticsearch.action.admin.cluster.health.ClusterHealthRequest;
import org.elasticsearch.action.admin.cluster.health.ClusterHealthResponse;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.*;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.client.license.GetBasicStatusResponse;
import org.elasticsearch.client.license.GetTrialStatusResponse;
import org.elasticsearch.cluster.health.ClusterHealthStatus;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.get.GetResult;
import org.elasticsearch.rest.RestStatus;

import java.util.Arrays;
import java.util.Set;

public class TestESHL {

    public static void main(String[] args) {
        String id = "001";
        String index = "test";
        TestESHL t = new TestESHL();
        try (
                RestHighLevelClient client = new RestHighLevelClient(
                        RestClient.builder(
                                new HttpHost("localhost", 9200, "http"),
                                new HttpHost("127.0.0.1", 9200, "http"),
                                new HttpHost("localhost", 9201, "http")
                        ))
        ) {
            t.info(client);
            t.license(client);
            if (t.nonExistsIndex(client, index)) t.createIndex(client, index);

//            t.postIndex(client, index, util.JsonUtils.toJSON(new TestJsonP()), id);
            println("DOC " + id + ", " + t.getDocument(client, index, id));

            t.getDocument(index, id);

//          if (t.existsIndex(client, name)) t.deleteIndex(client, name);
            t.indices(client);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    private String updateDocument(RestHighLevelClient client, String index, String json, String id) throws Exception {
        // 6.x UpdateRequest req = new UpdateRequest(index, "_doc", id);
        // 7.x UpdateRequest req = new UpdateRequest(index, id);
        UpdateRequest req = new UpdateRequest(index, id);
        req.doc(XContentType.JSON, json);
        UpdateResponse res = client.update(req, RequestOptions.DEFAULT);
        GetResult result = res.getGetResult();
        return result.source() != null ? new String(result.source()) : "NOT FOUND";
    }

    private String getDocument(String index, String id) {
        // Request req = new Request("GET", "/test/_doc/001");
        Request req = new Request("GET", "/" + index + "/_doc/" + id);
        req.addParameter("pretty", "true");

        try (RestClient client = RestClient.builder(
                new HttpHost("localhost", 9200, "http"),
                new HttpHost("127.0.0.1", 9200, "http"),
                new HttpHost("localhost", 9201, "http"))
                .build()
        ) {
            Response res = client.performRequest(req);
            RequestLine rl = res.getRequestLine();
            HttpHost host = res.getHost();
            int stsCode = res.getStatusLine().getStatusCode();
            println("HTTP STATUS CODE " + stsCode);
            Header[] headers = res.getHeaders();
            for (Header h : headers) println(Arrays.asList(h.getElements()) + " " + h.getName() + "=" + h.getValue());
            String body = EntityUtils.toString(res.getEntity());
            println(rl.getMethod() + " " + host + "" + rl.getUri());
            println(body);
            return body;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "NOT FOUND";
    }

    private String getDocument(RestHighLevelClient client, String index, String id) throws Exception {
        // 6.x GetRequest req = new GetRequest(index, "_doc", id);
        // 7.x GetRequest req = new GetRequest(index, id);
        GetRequest req = new GetRequest(index, id);
        GetResponse res = client.get(req, RequestOptions.DEFAULT);
        return res.getSourceAsBytes() != null ? new String(res.getSourceAsBytes()) : "NOT FOUND";
    }

    private int postIndex(RestHighLevelClient client, String index, String json, String id) throws Exception {
        IndexRequest req = new IndexRequest(index);
        req.id(id);
        req.source(json, XContentType.JSON);
        IndexResponse res = client.index(req, RequestOptions.DEFAULT);
        RestStatus sts = res.status();
        println("HTTP " + sts.getStatus());
        return sts.getStatus();
    }

    private boolean deleteIndex(RestHighLevelClient client, String index) throws Exception {
        DeleteIndexRequest req = new DeleteIndexRequest(index);
        AcknowledgedResponse res = client.indices().delete(req, RequestOptions.DEFAULT);
        println("index " + index + " isDeleted? " + res.isAcknowledged());
        return res.isAcknowledged();
    }

    private boolean nonExistsIndex(RestHighLevelClient client, String index) throws Exception {
        return !existsIndex(client, index);
    }

    private boolean existsIndex(RestHighLevelClient client, String index) throws Exception {
        GetIndexRequest req = new GetIndexRequest(index);
        boolean exists = client.indices().exists(req, RequestOptions.DEFAULT);
        println("index " + index + " isExists? " + exists);
        return exists;
    }

    private void createIndex(RestHighLevelClient client, String index) throws Exception {
        CreateIndexRequest req = new CreateIndexRequest(index);
        CreateIndexResponse res = client.indices().create(req, RequestOptions.DEFAULT);
        println("index " + res.index());
        println("isAck? " + res.isAcknowledged());
        println("isFrg? " + res.isFragment());
        println("isSrd? " + res.isShardsAcknowledged());
    }

    private void indices(RestHighLevelClient client) throws Exception {
        ClusterHealthRequest req = new ClusterHealthRequest();
        ClusterHealthResponse res = client.cluster().health(req, RequestOptions.DEFAULT);
        ClusterHealthStatus sts = res.getStatus();
        println(res.getClusterName() + " cluster health " + sts);
        log.info("{} cluster health {}", res.getClusterName(), sts);
        Set<String> indices = res.getIndices().keySet();
        indices.forEach(n -> println(n));
    }

    private void info(RestHighLevelClient client) throws Exception {
        // 6.x org.elasticsearch.action.main.MainResponse
        // 7.x org.elasticsearch.client.core.MainResponse
        org.elasticsearch.client.core.MainResponse info = client.info(RequestOptions.DEFAULT);
        println(info.getClusterName() + " on " + info.getNodeName());
        org.elasticsearch.client.core.MainResponse.Version v = info.getVersion();
    }

    private void license(RestHighLevelClient client) throws Exception {
        GetBasicStatusResponse basic = client.license().getBasicStatus(RequestOptions.DEFAULT);
        GetTrialStatusResponse trial = client.license().getTrialStatus(RequestOptions.DEFAULT);
        println("isBasic? " + basic.isEligibleToStartBasic());
        println("isTrial? " + trial.isEligibleToStartTrial());
    }

    private static void println(Object o) {
        System.out.println(o);
    }

    private final static org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(TestESHL.class);
}

