package test.hibernate;

import com.privasia.jpa.TUG_GPS;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.internal.SessionImpl;
import org.hibernate.service.ServiceRegistry;

import java.sql.Connection;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class HibernateSaveOrUpdate implements AutoCloseable {

    public static void main(String[] args) {
//        System.setProperty("org.jboss.logging.provider", "jdk");
//        java.util.logging.Logger.getLogger("org.hibernate").setLevel(java.util.logging.Level.WARNING);

        try (HibernateSaveOrUpdate t = new HibernateSaveOrUpdate(Arrays.asList(TUG_GPS.class))) {
            String name = "NIMBLE";
            int mmsi = 533005300;
            LocalDateTime ldt = LocalDateTime.parse("2020-03-16 00:00:05", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
            Date dt = Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
            double lon = 101.28893;
            double lat = 2.9250784;
            t.saveOrUpdate(new TUG_GPS(mmsi, dt, name, lon, lat));
            t.saveOrUpdate(new TUG_GPS(mmsi, dt, name, lon, lat));
        } catch (Exception e) {
            println(e.getMessage());
        }
    }

    public synchronized void execute(String sql) {
        sess.doWork(conn -> {
            Statement stmt = conn.createStatement();
            int rows = stmt.executeUpdate(sql);
            conn.commit();
            //log.info("{} rows\n{}", count, sql);
        });
    }

    public synchronized Connection getConnection() {
        return ((SessionImpl) sess).connection();
    }

    public synchronized void saveOrUpdate(Object entity) {
        Transaction tx = sess.beginTransaction();
        sess.saveOrUpdate(entity);
        tx.commit();
        sess.clear();
    }

    public HibernateSaveOrUpdate(List<Class> cls) {
        cfg = new Configuration().configure(getClass().getResource("/hibernate.cfg.xml"));
        for (Class c : cls) cfg.addAnnotatedClass(c);
        StandardServiceRegistryBuilder serviceRegistryBuilder = new StandardServiceRegistryBuilder();
        serviceRegistryBuilder.applySettings(cfg.getProperties());
        ServiceRegistry serviceRegistry = serviceRegistryBuilder.build();
        factory = cfg.buildSessionFactory(serviceRegistry);
        sess = factory.openSession();
    }

    public SessionFactory getFactory() {
        return factory;
    }

    public Session getSession() {
        return sess;
    }

    @Override
    public void close() {
        if (sess != null) sess.close();
    }

    private static void println(Object o) {
        System.out.println(o);
    }

    private Configuration cfg;
    private SessionFactory factory;
    private Session sess;
}
